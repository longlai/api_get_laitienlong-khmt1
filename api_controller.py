from odoo import http, _
from odoo.http import request
from odoo.addons.lc_rest_api.controllers.controllers import validate_token, generate_response, getRequestIP, serialization_data, toUserDatetime, _args, permission_check

class ApiGetData(http.Controller):
    # thong tin khach hang
    @validate_token
    @http.route(['/api/customer/search/', '/api/customer/search/<pos_id>'], methods=['GET'], type='http', auth='none', csrf=False)
    def model_pos_config(self, access_token, pos_id=None, **kw):
        if not permission_check('res.partner', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not pos_id:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['res.partner'].search_read(fields=['id', 'name','street','type','lang','function','barcode','city','state_id','zip','create_date','parent_id','phone','email','total_invoiced'], offset=_offset, limit=_limit, order=_order)
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'totalPages': len(data),
                    'page': (int)(_offset/_limit)+1,
                    'result': serialization_data(data)
                    
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                pos_id = int(pos_id)
                data = request.env['res.partner'].search_read(fields=['id', 'name','street','type','lang','function','barcode','city','state_id','zip','create_date','parent_id','phone','email','total_invoiced'],domain=[('id', '=', pos_id)])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids'])
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
# thoong tin hoa don
    @validate_token
    @http.route(['/api/get/posbill/', '/api/get/posbill/<name>'], methods=['GET'], type='http', auth='none', csrf=False)
    def model_pos_bill(self, access_token, name=None, **kw):
        if not permission_check('sale.order.line', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not name:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['sale.order.line'].search_read(fields=['id','order_partner_id','product_id','name','order_id','salesman_id','create_uid','discount'], offset=_offset, limit=_limit, order=_order)
                data2 = request.env['sale.order'].search_read(fields=['id','name','partner_id','warehouse_id','invoice_status','create_date'], offset=_offset, limit=_limit, order=_order)
                new_data = []
                for i in range(len(data)):    
                    check = True
                    for k in range(len(data2)):
                        if data[i]['order_partner_id'] == data2[k]['partner_id']:
                            check = False
                            new_list ={
                                'id': data2[k]['id'],
                                'name': data[i]['name'],
                                'order_id': data[i]['order_id'],
                                'salesman_id': data[i]['salesman_id'],
                                'create_uid': data[i]['create_uid'],
                                'discount': data[i]['discount'],
                                'warehouse_id': data2[k]['warehouse_id'],
                                'partner_id': data2[k]['partner_id'],
                                'product_id': data[i]['product_id'],
                                'invoice_status': data2[k]['invoice_status']
                            }
                            new_data.append(new_list)
                            break
                    if check == True:
                        new_data.append(data[i])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'totalPages': len(data),
                    'page': (int)(_offset/_limit)+1,
                    'result': serialization_data(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                name = str(name)
                data = request.env['sale.order.line'].search_read(fields=['id','order_partner_id','product_id','name','order_id','salesman_id','create_uid','discount'],domain=[('order_partner_id', '=', name)])
                data2 = request.env['sale.order'].search_read(fields=['id','name','partner_id','warehouse_id','invoice_status','create_date'],domain=[('partner_id', '=', name)])
                new_data = []
                for i in range(len(data)):    
                    check = True
                    for k in range(len(data2)):
                        if data[i]['order_partner_id'] == data2[k]['partner_id']:
                            check = False
                            new_list ={
                                'id': data2[k]['id'],
                                'name': data[i]['name'],
                                'order_id': data[i]['order_id'],
                                'salesman_id': data[i]['salesman_id'],
                                'create_uid': data[i]['create_uid'],
                                'discount': data[i]['discount'],
                                'warehouse_id': data2[k]['warehouse_id'],
                                'partner_id': data2[k]['partner_id'],
                                'product_id': data[i]['product_id'],
                                'invoice_status': data2[k]['invoice_status']
                            }
                            new_data.append(new_list)
                            break
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data+data2+new_data, restrict=['customer_facing_display_html', 'session_ids']),
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
# thong tin san pham
    @validate_token
    @http.route(['/api/get/pos_product/', '/api/get/pos_product/<pos_id>'], methods=['GET'], type='http', auth='none', csrf=False)
    def model_pos_product(self, access_token, pos_id=None, **kw):
        if not permission_check('sale.order.line', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not pos_id:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['sale.order.line'].search_read(fields=['product_id','company_id','name','product_uom_qty','price_total'], offset=_offset, limit=_limit, order=_order)
                data2 = request.env['product.template'].search_read(fields=['id','company_id','barcode'], offset=_offset, limit=_limit, order=_order)
                new_data = []
                for i in range(len(data)):    
                    check = True
                    for k in range(len(data2)):
                        if data[i]['company_id'] == data2[k]['company_id']:
                            check = False
                            new_list ={
                                'product_id': data[i]['product_id'],
                                'barcode': data2[k]['barcode'],
                                'name': data[i]['name'],
                                'product_uom_qty': data[i]['product_uom_qty'],
                                'price_total': data[i]['price_total']
                            }
                            new_data.append(new_list)
                            break
                    if check == True:
                        new_data.append(data[i])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'totalPages': len(data),
                    'page': (int)(_offset/_limit)+1,
                    'result': serialization_data(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                pos_id = str(pos_id)
                data = request.env['sale.order.line'].search_read(fields=['id','product_id','order_id','salesman_id','create_uid','discount','product_uom_qty','price_total'],domain=[('product_id', '=', pos_id)])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids'])
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
# tim kiem theo sdt
    @validate_token
    @http.route(['/api/customer/search/', '/api/customer/search/<phone>'], methods=['GET'], type='http', auth='none', csrf=False)
    def model_pos_config1(self, access_token, phone=None, **kw):
        if not permission_check('res.partner', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not phone:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['res.partner'].search_read(fields=['id', 'name','street','type','lang','function','create_date','parent_id','phone','email'], offset=_offset, limit=_limit, order=_order)
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data),
                    'tong khach hang': len(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                phone = str(phone)
                data = request.env['res.partner'].search_read(fields=['id', 'name','street','type','lang','function','create_date','parent_id','phone','email'],domain=[('phone', '=', phone)])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids'])
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
    @validate_token
    @http.route(['/api/order/index', '/api/order/index/<name>'], methods=['GET'], type='http', auth='none', csrf=False)
    def getdata_donhang(self, access_token, name=None, **kw):
        if not permission_check('sale.order', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not name:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['sale.order'].search_read(fields=['id','create_uid','user_id','name','create_date','commitment_date','date_order','confirmation_date','warehouse_id','invoice_status','carrier_id','carrier_code','partner_id'], offset=_offset, limit=_limit, order=_order)
                data2 = request.env['res.partner'].search_read(fields=['id','name','city','state_id','create_date','create_uid','phone','create_uid','commitment_date','cityid'], offset=_offset, limit=_limit, order=_order)
                new_data = []
                for i in range(len(data)):    
                    check = True
                    for k in range(len(data2)):
                        if data[i]['create_uid'] == data2[k]['create_uid']:
                            check = False
                            new_list ={
                                'id': data[i]['id'],
                                'create_uid': data[i]['create_uid'],
                                'user_id': data[i]['user_id'],
                                'name': data[i]['name'],
                                'create_date': data[i]['create_date'],
                                'date_order': data[i]['date_order'],
                                'confirmation_date': data[i]['confirmation_date'],
                                'commitment_date': data[i]['commitment_date'],
                                'warehouse_id': data[i]['warehouse_id'],
                                'invoice_status': data[i]['invoice_status'],
                                'carrier_id': data[i]['carrier_id'],
                                'partner_id': data[i]['partner_id'],
                                'phone': data2[k]['phone'],
                                'name': data2[k]['name'],
                                'city': data2[k]['city'],
                                'state_id': data2[k]['state_id'],
                                'create_date': data2[k]['create_date'],
                                'create_uid': data[i]['create_uid']
                            }
                            new_data.append(new_list)
                            break
                    if check == True:
                        new_data.append(data[i])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'don hang': serialization_data(new_data),
                    'ton don hang': len(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                pos_id = int(pos_id)
                data = request.env['sale.order'].search_read(fields=['id','order_id','salesman_id','create_uid','discount'],domain=[('id', '=', pos_id)])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids'])
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })